package controlador;

//import javax.swing.JOptionPane;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
/**
 * Esta clase solo continene el método main.
 * Incluye las annotations @SpringBootAplication, que configura el proyecto para
 * utilizar el framework SpringBoot,y @ComponentScan, que le indica a la
 * aplicación el nombre de los paquetes utilizados.
 * @author Leandro
 */

@SpringBootApplication
@ComponentScan(basePackages = {"DAO", "controlador", "DTO"})
public class Main {

    public static void main(String[] args) {
        //      JOptionPane.showMessageDialog(null, "Entro al main");
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
    }

}
