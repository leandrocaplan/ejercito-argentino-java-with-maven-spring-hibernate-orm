# Sistema de administración del Ejército Argentino, realizado en Java con los frameworks Spring y Hibernate

Esta aplicación consiste en una demostración básica de un desarrollo en web Java utilizando el partrón de diseño MVC, en la que se realizan operaciones elementales de CRUD (Create, Read, Update and Delete), defindo en Español como ABMC (Alta, Baja, Modificación y Consulta), así como una implemetación básica de un sistema de login y autenticación de usuarios. Este desarrollo implementa también el patrón de diseño DAO-DTO, el cual nos permite modularizar y estructurar el código de una manera mucho mas prolija y mantenible. La misma utiliza MySQL/MariaDB como motor de base de datos, así también los módulos pertinentes de Java para interactuar con la base de datos.

El proyecto original fue publicado como entrega final para una materia de la carrera de Tecnicatura Superior en Informática Aplicada, dictada en el Instituto Nacinal Superior del Profesorado Técnico (INSPT), el cual constituye una dependencia de la Universidad Tecnológica Nacional (UTN), de la República Argentina. Dado que dicho proyecto original fue publicado en el año 2019, 5 años antes de la presente publicación, se estima conveniente lanzar una nueva versión, en la cual se actualizan todas las herramientas y dependencias de software que constan de soporte técnico en la actualidad, siendo que las que fueron utilizadas en la versión original quedaron obsoletas hace ya mucho tiempo.

Esta aplicación se utiliza Maven como gestor de dependencias, conjuntamente con los siguientes frameworks para Java:

## Spring
Este framework es utilizado como sistema de inversión de control (IoC), el cual utiliza la inyección de dependencias para su funcionamiento, utilizando intensivamente "annotations" de Java dentro del código del programa, en las cuales se fundamenta la utilización de dicho framework para la codificación de las aplicaciones desarrolladas con esta tecnología. Este framework está optimizado para trabajar con Thymeleaf como motor de plantillas. De esta forma, los archivos de vista del proyecto, si bien su extensión es ".html", utilizan la sintaxis propia de Thymeleaf para generar su contenido de manera dinámica, utilizando la información que reciben del controlador para tal propósito.

Particularmente, dentro de este desarrollo en particular, Spring es utilizado intensivamente como enrutador (router) HTTP de nuestra aplicación, definiendo así la estructura de directorios que utiliza la misma para su funcionamiento, al ser ejecutada en el servidor web.

## Hibernate/JPA ORM
Este framework consiste en una herramienta de mapeo objeto-relacional (ORM), especifico para la plataforma Java. Este tipo de herramientas nos sirven para modelizar el contenido de una base de datos relacional (SQL) como objetos propios de Java. De esta forma, cada registro de una tabla de la base de datos puede ser modelizado como un objeto dentro de nuestra aplicación.

Esta herramienta nos permite prescindir, en gran medida, de la utilización del lenguaje SQL para realizar consultas a la base de datos. Si bien Hibernate nos ofrece un lenguaje de consulta llamado "Hibernate Query Language (HQL)", con una sintaxis similar al SQL pero con mayor soporte para la programación orientada a objetos, frecuentemente ni siquiera tendremos la necesidad de codificarlo manualmente, ya que este framework nos permite realizar el mapeo objeto-relacional simplemente mediante annotations de Java definidos en las clases correspondientes de las clases que definimos para este mapeo.

# Instalación
Esta versión de la aplicación fue desarrollada bajo el sistema operativo "Linux Mint 21.3", y está configurada para utilizar con el IDE "Apache NetBeans", version 20.
La misma funciona utilizando el JDK 11. Si bien, como toda aplicación Java, es multiplataforma, aquí describiremos los pasos para que la misma sea probada y ejecutada utilizando tales herramientas de software.

Es posible, seguramente, configurarla para que corra adecuadamente utilizando otros sistemas operativos, otras versiones de dicho IDE (o bien directamente otro IDE), o también otra versión del JDK. Sin embargo, aquí describiremos los pasos adecuados para que la aplicación sea compilada y ejecutada adecuadamente utilizando estas versiones del software mencionado.

## Instalando el JDK que necesitamos

Como primer paso, debemos asegurarnos de tener instalado y configurado correctamente el JDK 11.

Lo primero que siempre debemos hacer antes de instalar cualquier paquete via APT, es ejecutar en la terminal:
`sudo apt update`

Luego, desde nuestra terminal, ejecutamos el comando:
`sudo apt install openjdk-11-jdk`

Para asegurarnos que la versión de Java reconocida por defecto por el sistema, ejecutamos en la terminal:
```
leandro@leandro-Lenovo-B50-10:~$ update-alternatives --config java
Existen 2 opciones para la alternativa java (que provee /usr/bin/java).

  Selección   Ruta                                         Prioridad  Estado
------------------------------------------------------------
  0            /usr/lib/jvm/java-21-openjdk-amd64/bin/java   2111      modo automático
* 1            /usr/lib/jvm/java-11-openjdk-amd64/bin/java   1111      modo manual
  2            /usr/lib/jvm/java-21-openjdk-amd64/bin/java   2111      modo manual

Pulse <Intro> para mantener el valor por omisión [*] o pulse un número de selección: 1
```

Para corroborar si esta configuración tuvo éxito, ejecutamos luego:

```
leandro@leandro-Lenovo-B50-10:~$ java --version
openjdk 11.0.22 2024-01-16
OpenJDK Runtime Environment (build 11.0.22+7-post-Ubuntu-0ubuntu222.04.1)
OpenJDK 64-Bit Server VM (build 11.0.22+7-post-Ubuntu-0ubuntu222.04.1, mixed mode, sharing)
```
Si la versión de OpenJDK que nos muestra la terminal es la deseada (en este caso, la versión 11), significa que la hemos configurado correctamente.

## Instalando Maven

Para instalar Maven en su última versión, debemos ejecutar:
`sudo apt install maven`


## Instalando y configurando Apache NetBeans

En primer lugar debemos tener en cuenta que NO se recomienda la instalación de Apache NetBeans 20 mediante Flatpak. Esto es, la que utiliza el Gestor de Software de Linux Mint. Esto se debe a que la misma corre en un entorno virtual generado por Flatpak, y no reconocerá las instalaciones nativas que realizamos tanto del JDK como de Maven,lo cual conllevará a enormes dificultades en la configuración de estos módulos, hasta el punto de que la compilación del proyecto sea prácticamente irrealizable.

Tampoco se recomienda la utilización de Apache NetBeans 21, ya que esta versión tiene algunos defectos que nos dificultan la configuración de las herramientas utilizadas por Java para la gestión de dependencias, sobre todo Maven.

Se recomienda entonces, descargar el paquete ".deb" obtenido desde el sitio oficial de Apache NetBeans, el cual es posible encontrar en la sección "Older Realases" (Lanzamientos anteriores).

Una vez instalado el Apache NetBeans 20, debemos realizar ciertas modificaciones en su configuración. Vamos entonces al menú "Tools->Options". Allí seleccionamos el ícono correspondiente a "Java", y vamos a la pestaña "Maven". En el campo "Maven home", definimos el directorio de instalación de Maven en nuestro sistema operativo, generalmente:
> /usr/share/maven

En el campo "Default JDK", elegimos la opción "JDK 11 (System)"

Luego, en el campo "Global Excecution Options", definimos el texto:
> clean install -U -X

Debemos asegurarnos de que, en el directorio raíz de nuestro proyecto, el archivo "mvnw" pueda ejecutarse como un programa.
Para ello, desde la terminal, nos dirigimos al directorio raíz y ejecutamos el comando
> chmod +x mvnw

Como buena práctica, es conveniente que la carpeta correspondiente al directorio raíz, así como todas sus subcarpetas, tengan al usuario actual como propietario, y a la vez, permisos globales para su lectura y escritura.
Para ello, estando situados en el directorio raíz, ejecutamos:
> sudo chown <nom_usuario> -R .

> sudo chmod 777 -R .


Podemos asegurarnos de que la gestion de dependencias con Maven funcione sin errores independientemente de la configuración del IDE.
Para ello, desde el directorio raíz, podemos ejecutar:
>mvn clean install -U -X

Debemos ahora, configurar correctamente todas las plataformas de Java reconocidas por el IDE. Para ello vamos al menú "Tools->Java Platforms"
Si allí no vemos configurada la plataforma "JDK 11", la agregamos manualmente mediante el botón "Add platform...". Elegimos la opción "Java Standard Edition", y allí podremos visualizar en el navegador de archivos, el directorio donde se encuentra la plataforma que deseamos agregar.
Por defecto, este navegador nos dirige automáticamente al directorio "/usr/lib/jvm", en donde se encuentran generalmente las plataformas de JDK instaladas en el sistema. La nuestra será "/usr/lib/jvm/java-11-openjdk-amd64", por lo que elegimos esa opción, agragando así la plataforma deseada en caso de que no se encuentre agregada automáticamente.

Finalmente, para asegurarnos de que el proyecto se compile con el JDK que desamos (JDK 11), damos click derecho en el navegador de NetBeans, en la pestaña "Projects", sobre el proyecto que abrimos con NetBeans. En el menú desplegable que se nos muestra, elegimos la opción "Properties".
Allí, en el cuadro que nos aparece, vamos a "Build->Compile" y seleccionamos "JDK 11" en el campo "Java Platform".

## Configurando la base de datos

Para que el programa funcione correctamente, debemos tener instalada alguna versión de MySQL o en su defecto, de MariaDB. Resultará util tener instalada la herramienta "phpMyAdmin", la cual viene generalmente integrada con la instalación de Apache 2. Debemos asegurarnos de tener algun usuario en la base de datos con privilegios totales. En mi caso particular, dicho usuario es "root", y su password es también "root". En el caso de otra instalación, este usuario y contraseña pueden llegar a diferir.

Estas credenciaes estarán definidas en el archivo "final.cfg.xml", en mi caso, de la siguiente manera:
```
<property name="hibernate.connection.password">root</property>
<property name="hibernate.connection.username">root</property>
```

Estas credenciales pueden variar de acuerdo al usuario con privilegios totales configurado en su propia base de datos, tanto su nombre de usuario como su contraseña.

Luego, debemos importar el archivo "ejercito.sql", que se encuentra en el directorio raíz, de modo que tengamos construida la base de datos con el nombre correspondiente: "ejercito", y cargado allí al menos un usuario con privilegios totales sobre la aplicación (usuario de rango "Oficial").

Una vez hecho, podremos acceder al sistema con las credenciales:
> 1234 (Código)

> querty (Contraseña)

## Accediendo al sistema

Si todos los pasos anteriores fueron realizados correctamente, podremos acceder al sistema dando el boton "Run" en el IDE. Luego, tipeando en la barra de direcciones del navegador:
> localhost:8080

Allí veremos un formulario donde nos solicita las credenciales previamente mencionadas. Una vez ingresadas y validadas las mismas, ya podrá acceder a la plena funcionalidad de esta aplicación.
